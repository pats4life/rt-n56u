# README #

This is a fork of the rt-n56u project. The only changes to Padavan's source will be the ones necessary for compatibility of the firmware with the Belkin N 750 DB router. Any questions about the firmware, bugs, changelogs or how to compile it please visit https://bitbucket.org/padavan/rt-n56u. Questions specific to this router can be asked in this thread http://www.dd-wrt.com/phpBB2/viewtopic.php?t=139046.

You can find prebuilt images in the [download](https://bitbucket.org/pats4life/rt-n56u/downloads) section. For information about them please visit the dd-wrt.com thread.